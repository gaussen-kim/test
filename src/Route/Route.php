<?php

namespace Gaussenkim\Test\Route;


/**
 * Class Route
 *
 * @author: Wumeng - wumeng@gupo.onaliyun.com
 * @since: 2023-06-16 13:45
 */
class Route
{
    /** @var array $route */
    public static $route
        = [
            'message_center_sms_send'      => 'openapi/v1/message/send',
        ];
}